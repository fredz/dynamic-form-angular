import { Component, OnInit } from '@angular/core';
import { FieldService } from './service/field.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Constants } from './common/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:  [FieldService]
})
export class AppComponent implements OnInit {

  usersinfo: any[];
  fields: any;
  categories: any;
  dropdownOptions: any[];
  selectedCategory: any[];
  fieldsParticipant: any[];
  userSelected = Constants.DEFAULT_USER;

  showFiller = false;

  constructor(private fieldService: FieldService) {
  }

  /**
   *  load the necessary json datas
   */
  ngOnInit() {

    forkJoin([
      this.fieldService.getFormFields(),
      this.fieldService.getMenu(),
      this.fieldService.getUsersInfo(),
      this.fieldService.getStates()
    ])
      .subscribe(results => {
        this.fields = results[0];
        this.categories = results[1];
        this.usersinfo = results[2];
        this.dropdownOptions = results[3];

        const defaultUser = Constants.DEFAULT_USER; // we need a default user selected
        this.selectFields(defaultUser.loanParticipantId, defaultUser.participantTypeId); // select fields

      });
  }

  /**
   * get form fields  that belong to the user selected
   * PARTICIPANT_ID is the default user selected
  */
  selectFields(loanParticipantId, participantTypeId) {

    this.fields.forEach(element => {
      if (participantTypeId.toString() === element.participantTypeId.toString() &&
          loanParticipantId.toString() === element.loanParticipantId.toString()) {
        this.fieldsParticipant = element;
      }
    });
  }

  /**
   * when we select a user we need to get the form fields for the user selected
   * this method is being  executed from <menu-user> and <app-dynamic-form> component , this is  @output variable
   */
  handleChangeUser(event: any) {
    this.selectFields(event.loanParticipantId, event.participantTypeId);
  }

  /**
   * when it is pressed the button save, it is updating the percentage in the user list on right menu
   * this method is being  executed from  <app-dynamic-form> component , this is  @output variable
   */
  handleUpdatePercentage(event: any) {
    this.usersinfo.forEach(user => {
      if (user.loanParticipantId === event.loanParticipantId &&
          user.participantTypeId === event.participantTypeId ) {
            user.percentage = event.percentage;
      }
    });
  }

  /**
   * the 'arrow up' event, to move to the top of the page
   */
  scrollup() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}


