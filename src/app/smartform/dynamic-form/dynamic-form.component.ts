import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FieldControlService } from '../../service/field-control.service';
import { Constants } from '../../common/constants';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css'],
  providers:  [FieldControlService]
})
export class DynamicFormComponent implements OnInit, OnChanges {

  @Input() fields: any[];
  @Input() categories: any[];
  @Input() dropdownOptions: any[];
  @Input() fieldsParticipant: any[];
  @Output() updatePercentage = new EventEmitter<any>();

  form: FormGroup;

  constructor(private fieldService: FieldControlService) { }

  ngOnInit() {
  }

  /**
   * each time the inputs has changed we are loading FormGroup (using reactive form)
   */
  public ngOnChanges(): void {
    this.form = this.fieldService.toFormGroup(this.fieldsParticipant);
  }

  /**
   * when we press the button save the  percentage value have to be updated taking into account the fields required
   */
  saveFormFields() {

    const infoPercentage = this.fieldService.percentageInfo(this.fieldsParticipant, this.form); // get info to calculate the percentage
    const percentage = infoPercentage.countSucessRequired * 100 / infoPercentage.totalFieldRequired; // calculating the percentage

    // we are sending this info to the parent component using @output variable
    this.updatePercentage.emit({
      loanParticipantId: this.fieldsParticipant['loanParticipantId'],
      participantTypeId: this.fieldsParticipant['participantTypeId'],
      percentage: percentage
    });

  }
}
