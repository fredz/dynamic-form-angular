import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dynamic-field',
  templateUrl: './dynamic-field.component.html',
  styleUrls: ['./dynamic-field.component.css']
})
export class DynamicFieldComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() field: any;
  @Input() dropdownOptions: any;

  options: any[] = [];

  constructor() {

  }

  ngOnInit() {
    // if it is a dropdown field then load option for dropdown
    this.loadOptionsDropdown();
  }

  loadOptionsDropdown() {

    if (this.field.controlType === 'dropdownBox' || this.field.controlType === 'multiSelectBox') {
      this.options = this.getOptionsByFieldId(this.field.fieldId);
      // I am using the ID 15 for all dropdown because we need to add options for other IDs
      // this.options = this.getOptionsByFieldId('15');
    }
  }

  /**
   * loading options values for dropdown fields
   */
  getOptionsByFieldId(fieldId: string) {
    const dropdownOption =  this.dropdownOptions.find((element) => {
      if (element.fieldId.toString() === fieldId.toString()) {
        return element;
      }
    });

    if (dropdownOption === undefined) {
      return [];
    }else {
      return dropdownOption.options;
    }
  }

  /**
   * verify if the field input is ok
   */
  get isValid() {
    return this.form.controls[this.field.fieldId].valid;
  }

  /**
   * validate if there is a field required error
   */
  get errorRequired() {

    if (this.hasError() &&
        this.form.controls[this.field.fieldId].errors.required === true) {
      return true;
    }else {
      return false;
    }
  }

  /**
   * validate if the field has max length error
   */
  get errorMaxLength() {

    if (this.hasError() &&
        this.form.controls[this.field.fieldId].errors.maxlength !== undefined) {
      return true;
    }else {
      return false;
    }
  }

  /**
   * validate if the email valid
   */
  get errorEmail() {

    if (this.hasError() &&
        this.form.controls[this.field.fieldId].errors.email === true) {
      return true;
    }else {
      return false;
    }
  }

  hasError() {


    /*if (this.field.fieldId === '28' ) {
      console.log(this.form.controls[this.field.fieldId]);
    }*/


    if (!this.form.controls[this.field.fieldId].valid &&
        this.form.controls[this.field.fieldId].errors !== null) {

      return true;
    }else {
      return false;
    }
  }


}
