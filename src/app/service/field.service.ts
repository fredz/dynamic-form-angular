import { Injectable } from '@angular/core';

import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class FieldService {

  constructor(private http: Http) {

  }

  public getUsersInfo(): Observable<any> {
    return this.http.get('assets/data/users.json')
                    .map((res: any) => res.json());
  }


  public getFormFields(): Observable<any> {
    return this.http.get('assets/data/populate-form-val.json')
                    .map((res: any) => res.json());
  }


  public getMenu(): Observable<any> {
    return this.http.get('assets/data/menu.json')
                    .map((res: any) => res.json());
  }

  public getStates(): Observable<any> {
    return this.http.get('assets/data/field-metadata.json')
                    .map((res: any) => res.json());
  }

}
