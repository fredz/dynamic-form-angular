import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Injectable()
export class FieldControlService {

  constructor() { }

  /**
   * iterate all fields and set the the validator for each field according the json data
   */
  toFormGroup(formFields: any ) {

    let fieldValue;

    const group: any = {};
    formFields.data.forEach(categories => {
      categories.fields.forEach(field => {

        // value for textbox
        fieldValue = field.fieldValue;

        // value for dropdown
        if (field.controlType === 'dropdownBox') {
          fieldValue = field.fieldValue[0];
        }

         group[field.fieldId] = new FormControl({value: (fieldValue || ''), disabled: !field.enable}, this.mapValidators(field));
      });
    });

    return new FormGroup(group);
  }

  /**
   * validate field required and maxLength
   */
  private mapValidators(element) {
    const formValidators = [];

    // validate field required
    if (element.required === true) {
      formValidators.push(Validators.required);
    }

    // validate maxlength
    if (element.maxLength !== undefined && element.maxLength !== '') {
      formValidators.push(Validators.maxLength(element.maxLength));
    }

    // validate field type email
    if (element.email === true) {
      formValidators.push(Validators.email);
    }

    return formValidators;
  }

  /**
   * get the info to calculate the percentage
   */
  percentageInfo(fieldsParticipant, formGroup) {

    let countSucessRequired = 0; // number of valid fields that are required
    let totalFieldRequired = 0; // total field required

    fieldsParticipant['data'].forEach(categories => {
      categories.fields.forEach(field => {

        if (field.required) {

          totalFieldRequired ++;

          if (formGroup.controls[field.fieldId].valid) {
            countSucessRequired ++;
          }
        }

      });
    });

    return {
      countSucessRequired,
      totalFieldRequired
    };
  }

}
