import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Constants } from '../common/constants';

@Component({
  selector: 'app-menu-user',
  templateUrl: './menu-user.component.html',
  styleUrls: ['./menu-user.component.css']
})
export class MenuUserComponent implements OnInit {

  @Input() users: any[];
  @Output() changeUser = new EventEmitter<any>();
  @Input() userSelected: any;

  constructor() { }

  ngOnInit() {
  }

  onChangeUser(user) {
    this.changeUser.emit({
      loanParticipantId: user.loanParticipantId,
      participantTypeId: user.participantTypeId
    });

    this.userSelected = user;
  }

}
