export class Constants {

  //  DEFAULT ROLE (Borrower)
  public static get DEFAULT_USER(): any {
    return {
      'loanParticipantId': 5,
      'participantTypeId': 1,
      'participantType': 'Borrower',
      'username': 'Lenny Jones'
    };
  }

}
